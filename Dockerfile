FROM debian:buster-slim

ENV FIREFOX_VERSION 76.0
ENV GECKODRIVER_VERSION v0.26.0
ENV CHROMEDRIVER_VERSION 81.0.4044.138

# Add Google Chrome repository
RUN apt update -qq && apt install -y wget gnupg2 \
	&& wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
	&& echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | tee /etc/apt/sources.list.d/google-chrome.list

# Install depencies
RUN apt update -qq && apt install -y \
	git \
	google-chrome-stable \
	openssl \
	ca-certificates \
	libdbus-glib-1-dev \
	python3 \
	python3-pip \
	unzip \
	&& rm -rf /var/lib/apt/lists/* \
	&& pip3 install --upgrade pip && pip3 install robotframework-seleniumlibrary

# Install Firefox
RUN wget https://ftp.mozilla.org/pub/firefox/releases/$FIREFOX_VERSION/linux-x86_64/en-US/firefox-$FIREFOX_VERSION.tar.bz2 \
    && tar xjf firefox-$FIREFOX_VERSION.tar.bz2 -C /opt/ \
    && rm -f firefox-$FIREFOX_VERSION.tar.bz2 \
    && ln -s /opt/firefox/firefox /usr/bin/firefox

# Download and setup a Geckodriver
RUN wget -q "https://github.com/mozilla/geckodriver/releases/download/$GECKODRIVER_VERSION/geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz" \
    && tar xzf geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz \
    && rm -f geckodriver-$GECKODRIVER_VERSION-linux64.tar.gz \
    && chmod +x geckodriver \
    && mv geckodriver /usr/local/bin

# Download and setup a Chromedriver
RUN wget https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip \
	&& unzip chromedriver_linux64.zip \
	&& chmod +x chromedriver \
	&& mv -f chromedriver /usr/bin/chromedriver \
	&& rm -f chromedriver_linux64.zip \
	&& apt autoremove -y unzip

# Copy a starting script from the repository and add execute rights
COPY run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh

# Define a starting script
CMD ["run.sh"]
