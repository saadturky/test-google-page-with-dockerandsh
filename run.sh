#!/bin/bash

set -e

# Setup Google Chrome conf. (no need to create new webdrivers)
NEW_LINE='exec -a "$0" /opt/google/chrome/chrome --user-data-dir --no-sandbox "$@"'
sed -i '$d' /usr/bin/google-chrome
echo ${NEW_LINE} >> /usr/bin/google-chrome
echo "--no-sandbox lisätty!"

# Declare default values
DEFAULT_LOG_LEVEL="INFO"
DEFAULT_TEST_RUN_TYPE="robot"

# Optional parameters, if no new value is given as an argument at the script startup, the default value will be used
LOG_LEVEL=${LOG_LEVEL:-$DEFAULT_LOG_LEVEL} #(INFO, TRACE, DEBUG, WARN, NONE)
TEST_RUN_TYPE=${TEST_RUN_TYPE:-$DEFAULT_TEST_RUN_TYPE} #(robot, pybot, sh)


# Necessary parameters
if [[ -z ${ROBOT_TESTS} ]];
  then
    echo "Error: Please specify the absolute path to the robot test as an environment variable: ROBOT_TESTS"
    exit 1
fi

if [[ -z ${OUTPUT_DIR}  ]];
  then
    echo "Error: Please specify the absolute path to the wanted output directory as an environment variable: OUTPUT_DIR"
    exit 1
fi


# Execute tests
echo -e "Executing robot tests at log level: ${LOG_LEVEL}"

if [ "$TEST_RUN_TYPE" = "sh" ]
then
    sh ${ROBOT_TESTS}
    mv ./report.html ${OUTPUT_DIR}/report.html
    mv ./output.xml ${OUTPUT_DIR}/output.xml
    mv ./log.html ${OUTPUT_DIR}/log.html
else
    ${TEST_RUN_TYPE} --loglevel ${LOG_LEVEL} --outputdir ${OUTPUT_DIR} ${ROBOT_TESTS}
fi
