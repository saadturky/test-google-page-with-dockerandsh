*** Settings ***
Library    SeleniumLibrary
Suite Teardown  Close Browser

*** Keywords ***
Start Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome      chrome_options=${chrome_options}


*** Test Cases ***
Hausta Suoraan Sivulle
    Start Chrome
	Go To	    	https://en.wikipedia.org/wiki/
    Capture Page Screenshot
    Input Text            searchInput            finland
    Click Element            searchButton
    Wait Until Page Contains    Finland                timeout=10
    Title Should Be            Finland - Wikipedia
    Capture Page Screenshot
    [Teardown]            Close All Browsers
